package dao

import (
	"fmt"
	"time"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

// X 全局DB
var db *gorm.DB

//全局引擎组
//var x *xorm.EngineGroup

func New(dsn string) {

	var err error

	db, err = gorm.Open("mysql", dsn)
	if err != nil {
		panic(err.Error())
	}

	
	db.DB().SetMaxOpenConns(50)
	db.DB().SetMaxIdleConns(10)
	db.DB().SetConnMaxLifetime(1800 * time.Second)
	//db.ShowSQL(dbConf.Debug) //调试 展示sql语句
	//db.ShowExecTime(dbConf.Debug) //显示SQL执行时间
	
	err = db.DB().Ping()
	if err != nil {
		
		panic(fmt.Sprintf("database master err:%s", err))
	} else {
		fmt.Println("PING MASTER  DATABASE  SUCCESS!!!")
	}
	
}

func Close() {
	db.Close()
}
