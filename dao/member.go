package dao

import (
	"members/libs/rpc"
	"members/colf/types"
)

func MemberInsert(m *types.Member) rpcx.Result {
	
	res := rpcx.Result{
		State : true,
		Data  : "",
	}
	err := db.Create(m).Error

	if err != nil {
		
		res.Data  = err.Error()
		res.State = false
		
	}
	return res
}
