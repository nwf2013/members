package main

import (
	"os"
	"fmt"
	"log"
	"members/dao"
	"members/libs/rpc"
	"members/colf/types"
	//"members/libs/store"
	"members/libs/logger"
	"members/libs/session"
	"members/libs/discovery"
	"github.com/json-iterator/go"
	"github.com/valyala/fasthttp"
)

type config struct {

	Session struct {
		Addr string `json:"addr"`
		Auth string `json:"auth"`
	} `json:"session"`
	Fluent struct {
		Host string `json:"host"`
		Port int    `json:"port"`
	} `json:"fluent"`
	Db string `json:"db"`
	Addr string `json:"addr"`
}

var conf config
var cjson = jsoniter.ConfigCompatibleWithStandardLibrary



func parseConfig(addrs, key string) {
	
	var sessionConf session.Config

	discovery.New(addrs)

	resp, err := discovery.Get(key)
	if err != nil {
		log.Panic(err)
	}
	//fmt.Println("lease:", resp.Kvs)

	item := resp.Kvs[0]
	fmt.Printf("%s : %s \n", item.Key, item.Value)
		
	cjson.Unmarshal(item.Value, &conf)

	sessionConf.Addr = conf.Session.Addr
	sessionConf.Auth = conf.Session.Auth
	
	session.New(sessionConf)
	
}

func Insert(ctx *fasthttp.RequestCtx) {

	m := new(types.Member)
	d := ctx.PostBody()

	err := m.UnmarshalBinary(d)
	if err != nil {
		fmt.Println(err)
		return
	}

	res := dao.MemberInsert(m)
	fmt.Println(m)
	
	body, err := res.MarshalBinary()
	if err != nil {
		fmt.Println(err)
		return
	}

	ctx.SetBody(body)
}

func main() {

	fmt.Println("os.Args = ", os.Args[1])
	parseConfig(os.Args[1], os.Args[2])

	dao.New(conf.Db)
	logger.New(conf.Fluent.Host, conf.Fluent.Port)

	discovery.Registry("member", conf.Addr, 5)

	rpcx.NewServer()
	rpcx.Def("/Insert", Insert)

	defer func () {
		
		discovery.UnRegistry("member", conf.Addr)

		dao.Close()
		logger.Close()
		discovery.Close()
	}()

	rpcx.Start(conf.Addr)
}